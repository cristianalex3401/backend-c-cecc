const {
    queueCreate,
    queueDelete,
    queueUpdate,
    queueFindOne,
    queueView,
} = require("../src/Adapters/index");

async function Create() {
    try {
        const job = queueCreate.add({ name, age, color });

        const result = await job.finished();
        console.log(result);
    } catch (error) {
        console.log(error);
    }
}
async function Delete() {
    try {
        const job = queueDelete.add({ id });

        const result = await job.finished();
        console.log(result);
    } catch (error) {
        console.log(error);
    }
}
async function Update() {
    try {
        const job = queueUpdate.add({ name, age, color, id });

        const result = await job.finished();
        console.log(result);
    } catch (error) {
        console.log(error);
    }
}
async function FindOne() {
    try {
        const job = queueFindOne.add({ id });

        const result = await job.finished();
        console.log(result);
    } catch (error) {
        console.log(error);
    }
}
async function View() {
    try {
        const job = queueView.add({});

        const result = await job.finished();
        console.log(result);
    } catch (error) {
        console.log(error);
    }
}
async function main() {
    View();
}
