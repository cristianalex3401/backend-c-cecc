const { sequelize } = require("../settings");
const { DataTypes } = require("sequelize");

const Model = sequelize.define("curso", {
    name: { type: DataTypes.STRING },
    age: { type: DataTypes.BIGINT },
    color: { type: DataTypes.STRING },
});

async function SyncDB() {
    try {
        await Model.sync();
    } catch (error) {
        console.log(error);
    }
}

module.exports = { SyncDB, Model };
