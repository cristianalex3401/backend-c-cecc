const dotenv = require("dotenv");
const { Sequelize } = require("sequelize");
dotenv.config();

const redis = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
};

const internalError = "No podemos procesar tu solicitud en estos momentos!!!";

const sequelize = new Sequelize({
    host: process.env.POSTGRES_HOST,
    port: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_HOST,
    username: process.env.POSTGRES_HOST,
    password: process.env.POSTGRES_HOST,
    dialect: "postgres",
});

module.exports = { redis, internalError, sequelize };
