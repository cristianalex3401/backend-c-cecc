const Controllers = require("../Controllers");

// SERVICES
// En el Service, es donde hacemos la logica ...
async function Create({ name, age, color }) {
    try {
        let { statusCode, data, message } = await Controllers.Create({
            name,
            age,
            color,
        });
        return { statusCode, data, message };
    } catch (error) {
        // console.log(error, '--> este es el error que se genera en Services')
        console.log({ step: "service Create", error: error.toString() });
        return { statusCode: 500, data: null, message: error.toString() };
    }
}
async function Delete({ id }) {
    try {
        let { statusCode, data, message } = Controllers.Delete({
            where: { id },
        });
        return { statusCode, data, message };
    } catch (error) {
        // console.log(error, '--> este es el error que se genera en Services')
        console.log({ step: "service Delete", error: error.toString() });
        return { statusCode: 500, data: null, message: error.toString() };
    }
}
async function Update({ name, age, color, id }) {
    try {
        let { statusCode, data, message } = Controllers.Update({
            name,
            age,
            color,
            id,
        });
        return { statusCode, data, message };
    } catch (error) {
        // console.log(error, '--> este es el error que se genera en Services')
        console.log({ step: "service Services", error: error.toString() });
        return { statusCode: 500, data: null, message: error.toString() };
    }
}
async function FindOne({ id }) {
    try {
        let { statusCode, data, message } = Controllers.FindOne({
            where: { id },
        });
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "service FindOne", error: error.toString() });
        return { statusCode: 500, data: null, message: error.toString() };
    }
}
async function View({}) {
    try {
        let { statusCode, data, message } = Controllers.View({});
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "service View", error: error.toString() });
        return { statusCode: 500, data: null, message: error.toString() };
    }
}
module.exports = { Create, Delete, Update, FindOne, View };
