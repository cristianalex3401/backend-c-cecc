const Services = require("../Services");
const { internalError } = require("../settings");
const {
    queueView,
    queueCreate,
    queueDelete,
    queueFindOne,
    queueUpdate,
} = require("./index");

queueView.process(async function (job, done) {
    try {
        const {} = job.data; // LO QUE NOS VAN A ENVIAR A NOSOTROS...
        let { statusCode, data, message } = await Services.View({});
        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapter queueView", error: error.toString() });
        done(null, { statusCode: 500, data: null, message: internalError });
    }
});
queueCreate.process(async function (job, done) {
    try {
        const { name, age, color } = job.data; // LO QUE NOS VAN A ENVIAR A NOSOTROS...
        let { statusCode, data, message } = await Services.Create({
            name,
            age,
            color,
        });
        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapter queueCreate", error: error.toString() });
        done(null, { statusCode: 500, data: null, message: internalError });
    }
});
queueDelete.process(async function (job, done) {
    try {
        const { id } = job.data; // LO QUE NOS VAN A ENVIAR A NOSOTROS...
        let { statusCode, data, message } = await Services.Delete({ id });
        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapter queueDelete", error: error.toString() });
        done(null, { statusCode: 500, data: null, message: internalError });
    }
});
queueFindOne.process(async function (job, done) {
    try {
        const { id } = job.data; // LO QUE NOS VAN A ENVIAR A NOSOTROS...
        let { statusCode, data, message } = await Services.FindOne({ id });
        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapter queueFindOne", error: error.toString() });
        done(null, { statusCode: 500, data: null, message: internalError });
    }
});
queueUpdate.process(async function (job, done) {
    try {
        const { name, age, color, id } = job.data; // LO QUE NOS VAN A ENVIAR A NOSOTROS...
        let { statusCode, data, message } = await Services.Update({
            name,
            age,
            color,
            id,
        });
        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapter queueUpdate", error: error.toString() });
        done(null, { statusCode: 500, data: null, message: internalError });
    }
});
