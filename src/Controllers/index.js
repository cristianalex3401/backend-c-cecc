const { Model } = require("../Models");

async function Create({ name, age, color }) {
    try {
        let instance = Model.create(
            { name, age, color },
            { fields: ["name", "age", "color"] }
        );
        return {
            statusCode: 200,
            data: instance.toJSON(),
            message: "Todo OK",
        };
    } catch (error) {
        console.log({ step: "Controllers.Create", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
}
async function Delete({ where = {} }) {
    try {
        await Model.destroy({ where });
        return { statusCode: 200, data: "Todo OK" };
    } catch (error) {
        console.log({ step: "Controllers.Delete", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }
}
async function Update({ name, age, color, id }) {
    try {
        let instance = await Model.update(
            { name, age, color },
            { where: { id } }
        );
        return { statusCode: 200, data: instance[1][0].toJSON() };
    } catch (error) {
        console.log({ step: "Controllers.Update", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
}
async function FindOne({ where = {} }) {
    try {
        let instance = await Model.findOne({ where });
        return { statusCode: 200, data: instance.toJSON(), message: "Todo OK" };
    } catch (error) {
        console.log({ step: "Controllers.FindOne", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
}
async function View({ where = {} }) {
    try {
        let instances = await Model.findAll({ where });

        return {
            statusCode: 200,
            data: instances,
            message: "Todo OK",
        };
    } catch (error) {
        console.log({ step: "Controllers.View", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }
}

module.exports = { Create, Update, Delete, View, FindOne };
